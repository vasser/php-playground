<?php
class Test implements Serializable {

    public $_val1 = 'lorem ipsum';
    public $_val2 = 'dolores maya';
    public $_val3 = array(1, 2, 'a', 'b');
    public $_val4 = 1.25;
    public $_val5 = null;

    public function __construct() {
        $this->_val5 = time();
    }

    public function __sleep() {
        return array('_val1');
    }

    public function __wakeup() {
        $this->showVal(5);
    }

    public function showVal($param) {
        $str = '_val' . $param;
        echo $this->$str . "\r\n";
    }

    public function serialize() {
        return serialize(
            array(
                '_val5' => $this->_val5
            )
        );
    }
    public function unserialize($data) {
        foreach ( unserialize($data) as $key => $value) {
            $this->$key = $value;
        }
    }

}

$obj = new Test();

echo "<p>val5: " . $obj->_val5 . '</p>';

echo "<p>new obj:</p>";
var_dump($obj);

$objSer = serialize($obj);

echo "<p>ser obj:</p>";
var_dump($objSer);

$objNew = unserialize($objSer);

echo "<p>unser obj:</p>";
var_dump($objNew);

echo "<p>val5: " . $objNew->_val5 . '</p>';