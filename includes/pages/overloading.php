<?php
class Test {

    public $_val1 = 'lorem ipsum';
    public $_val2 = 'dolores maya';
    public $_val3 = array(1, 2, 'a', 'b');
    public $_val4 = 1.25;

    public function __call($name, $arguments) {
        echo '<p>This is called function "' . $name . '", ';
        if(empty($arguments) === false) {
            echo 'with args: ' . implode(', ', $arguments);
        } else {
            echo 'without args';
        }
        echo '</p>';
    }

    public static function __callStatic($name, $arguments) {
        echo '<p>This is called <u>static</u> function "' . $name . '", ';
        if(empty($arguments) === false) {
            echo 'with args: ' . implode(', ', $arguments);
        } else {
            echo 'without args';
        }
        echo '</p>';
    }

    public function __set($name, $value) {
        $this->_val3[$name] = $value;
    }

    public function __get($name) {
        echo '<p>Getting '.$name.'</p>';
        if(array_key_exists($name, $this->_val3)) {
            return $this->_val3[$name];
        } else {
            return false;
        }
    }

    public function __isset($name) {
        echo '<p>Is '.$name.' already set?</p>';
        return isset($this->_val3[$name]);
    }

    public function __unset($name) {
        echo '<p>Unsetting '.$name.' property</p>';
        unset($this->_val3[$name]);
    }

}

$obj = new Test();

echo '<h3><code>__call</code></h3>';
$obj->runTest('some_func', '1', 'b');
echo '<hr>';

echo '<h3><code>__callStatic</code></h3>';
Test::runTest('some_func_static', '2', 'c');
Test::runTest();
echo '<hr>';

echo '<h3><code>__set</code></h3>';
$obj->_newProp = 1;
var_dump($obj->_newProp);
echo '<hr>';

echo '<h3><code>__get</code></h3>';
$obj->_newProp;
var_dump($obj->_newProp);
echo '<hr>';

echo '<h3><code>__unset</code></h3>';
unset($obj->_newProp);
var_dump($obj->_newProp);
echo '<hr>';

echo '<h3><code>__isset</code></h3>';
var_dump(isset($obj->_newProp));
$obj->_newProp = 2;
var_dump(isset($obj->_newProp));
var_dump($obj->_newProp);
echo '<hr>';