<?php
class A {

    private static $sfoo = 1;
    private $ifoo = 2;

}

$cl1 = static function() {
    return A::$sfoo;
};

$cl2 = function() {
    return $this->ifoo;
};

echo '<h3><code>Closure::bind</code></h3>';
$bcl1 = Closure::bind($cl1, null, 'A');
$bcl2 = Closure::bind($cl2, new A(), 'A');
echo "<p>" . $bcl1(), "</p>";
echo "<p>" . $bcl2(), "</p>";
echo '<hr>';

Class B {

    public $baz = 'a';
    public $bar = 'b';

    public function __construct($baz, $bar) {
        $this->baz = $baz;
        $this->bar = $bar;
    }

    function getClosure() {
        return function() {
            return $this->baz . ' AND ' . $this->bar;
        };
    }

}

echo '<h3><code>Closure::bindTo</code></h3>';
$b = new B('q', 'w');
$c = new B('z', 'x');
$cl3 = $b->getClosure();
$bcl3 = $cl3->bindTo($c);
echo '<p>'.$bcl3().'</p>';
echo '<hr>';