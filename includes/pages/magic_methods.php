<?php
class Test {

    public $_val1 = 'lorem ipsum';
    public $_val2 = 'dolores maya';
    public $_val3 = array(1, 2, 'a', 'b');
    public $_val4 = 1.25;
    public $_val5 = null;

    public function __construct() {
        $this->_val5 = time();
    }

    public function __sleep() {
        return array('_val5' => $this->_val5);
    }

    public function __wakeup() {
        $this->showVal(5);
    }

    public function showVal($param) {
        $str = '_val' . $param;
        echo '<p>' . $this->$str . '</p>';
    }

    public function __clone() {
        unset($this->_val3['_newProp']);
        $this->_val4 = ++$this->_val4;
        $this->_val5 = time()+1;
    }

}

$obj = new Test();

echo '<h3><code>__sleep</code></h3>';
echo '<p>new obj:</p>';
var_dump($obj);
var_dump($obj->__sleep());
$obj->__sleep();
echo '<p>obj after sleep:</p>';
var_dump($obj);
$obj->showVal(5);
echo '<hr>';

echo '<h3><code>__wakeup</code></h3>';
$obj->__wakeup();
echo '<p>obj after wakeup:</p>';
var_dump($obj);
echo '<hr>';

echo '<h3><code>__clone</code></h3>';
$objClone = clone $obj;
var_dump($obj);
var_dump($objClone);
echo '<hr>';