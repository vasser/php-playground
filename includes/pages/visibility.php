<?php
class Test {

    public $_public = 'Public property';
    protected $_protected = 'Protected property';
    private $_private = 'Private property';

    public function printProperties() {

        echo '<ul>' .
            '<li>'.$this->_public.'</li>' .
            '<li>'.$this->_protected.'</li>' .
            '<li>'.$this->_private.'</li>' .
            '</ul>';

    }

    protected function printProtected() {

        echo '<p>'.$this->_protected.'</p>';

    }

    private function printPrivate() {

        echo '<p>'.$this->_private.'</p>';

    }

    public function printProAndPri() {

        $this->printProtected();
        $this->printPrivate();

    }

}

class Test2 extends Test {

    protected $_protected = 'Protected Child Property';
    private $_private = 'Private Child Property';

    private function printPrivate() {

        echo '<p>'.$this->_private.'</p>';

    }

    public function printProAndPri() {

        $this->printProtected();
        $this->printPrivate();

    }

}

$test = new Test();

echo '<h3>Direct <u>Public</u> Properties call</h3>';
echo '<p>'.$test->_public.'</p>';
echo '<hr>';

echo '<h3>Method <u>All</u> Properties call</h3>';
$test->printProperties();
echo '<hr>';

echo '<h3>Method <u>Protected And Private</u> Properties call</h3>';
$test->printProAndPri();
echo '<hr>';

$test2 = new Test2();

echo '<h3>Method <u>All</u> Child Properties call</h3>';
$test2->printProperties();
echo '<hr>';

echo '<h3>Method <u>Protected And Private</u> Child Properties call</h3>';
$test2->printProAndPri();
echo '<hr>';