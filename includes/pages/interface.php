<?php

interface a {

    const aconst = 'const_interface_a';

    public function foo($param);
    public function bar($param1, $param2);
    public function baz($param1, $param2, $param3);

}

interface b extends a {

    const bconst = 'const_interface_b';

}

class Test implements b {

    public function __construct() {
        echo '<p>'.a::aconst.'</p>';
        echo '<p>'.b::bconst.'</p>';
    }

    function foo($param) {
        echo '<p>Foo: '.$param.'</p>';
    }

    public function bar($param1, $param2) {
        echo '<p>Bar: '.$param1.', '.$param2.'</p>';
    }

    public function baz($param1, $param2, $param3) {
        echo '<p>Baz: '.$param1.', '.$param2.', '.$param3.'</p>';
    }

    function anotherFunc() {
        echo '<p>This func is out of interface "a"</p>';
    }

}

$test = new Test();
$test->foo('lorem');
$test->bar('lorem', 'ipsum');
$test->baz('lorem', 'ipsum', 'dolore');
$test->anotherFunc();