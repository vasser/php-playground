<?php
class Test {

    public function __invoke($param) {
        echo '<ol>';
        for($i = 0; $i < strlen($param); $i++) {
            echo '<li>' .  $param[$i] . '</li>';
        }
        echo '</ol>';
        var_dump($param);
    }

}

$obj = new Test();

echo '<h3><code>__invoke</code></h3>';
if(is_callable($obj) === true) {
    $obj('population');
} else {
    echo 'this class is not callable<br>';
}
echo '<hr>';