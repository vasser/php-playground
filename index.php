<?php
define('ROOT', $_SERVER['DOCUMENT_ROOT']);
define('ROOT_INC', ROOT . '/includes/');
define('ROOT_INC_PAGES', ROOT_INC . '/pages/');
define('SITE', $_SERVER['SERVER_NAME']);
define('SITE_R', 'http://' . SITE);
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>PHP Playground</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

    <!-- Custom styles for this template -->
    <link href="css/sticky-footer-navbar.css" rel="stylesheet">

  </head>

  <body>

      <div class="navbar navbar-default navbar-fixed-top" role="navigation">
          <div class="container">
              <div class="navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                      <span class="sr-only">Toggle navigation</span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="/">Project name</a>
              </div>
              <div class="collapse navbar-collapse">
                  <ul class="nav navbar-nav">

<?php
// get current page
$page = htmlspecialchars($_GET['p']);

// get page name
function getPageName($page) {
    $name = str_replace(array('_', '.php'), array(' ', ''), $page);
    return ucfirst($name);
};

// build menu with the closure
$pages = scandir(ROOT_INC_PAGES);
$cb = function($value, $key) use ($page, &$menu) {
    if(!preg_match('/^\.{1,2}$/', $value)) {
        $value = str_replace('.php', '', $value);
        $menu .= '<li'.(($page == $value) ? ' class="active"' : '').'><a href="'.SITE_R.'/?p='.$value.'">' . getPageName($value) . '</a></li>';
    }
};

array_walk($pages, $cb);
echo $menu;
?>
                  </ul>
              </div><!--/.nav-collapse -->
          </div>
      </div>

      <div class="container">

<?php
// display content
if($page && !empty($page)) {

    $page_root = ROOT_INC_PAGES . $page . '.php';

    if(file_exists($page_root)) {
        echo '<div class="page-header">
            <h1>'.getPageName($page).'</h1>
        </div>';
        include_once $page_root;
    } else {
        echo '<div class="jumbotron">
            <h1>Page not found</h1>
        </div>';
    }

} else {
    echo '<div class="jumbotron">
        <h1>Choose the test page</h1>
    </div>';

}
?>

        </div>

        <div class="footer">
            <div class="container">
                <p class="text-muted"><span class="glyphicon glyphicon-heart"></span> PHP</p>
            </div>
        </div>


      <!-- Bootstrap core JavaScript
      ================================================== -->
      <!-- Placed at the end of the document so the pages load faster -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
      <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
  </body>
</html>